# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/hlf-legal-ledger/fabric-samples/bin" ] ; then
    PATH="/workspaces/hlf-legal-ledger/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# # Delete existing artifacts
# rm -rf ./crypto-config
# rm genesis.block mychannel.tx
# rm -rf ../../channel-artifacts/*
rm legalsolution-genesis.block legalsolution-channel.tx
#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD

# Generate the genesis block for the University Consortium Orderer
configtxgen -profile LegalSolutionOrdererGenesis -channelID ordererchannel -outputBlock legalsolution-genesis.block

# Create the channel LegalSolutionsChannel
configtxgen -outputCreateChannelTx ./legalsolution-channel.tx -profile LegalSolutionChannel -channelID legalsolutionchannel

configtxgen -outputAnchorPeersUpdate LegalSolutionIncAnchors.tx -profile LegalSolutionChannel -channelID legalsolutionChannel -asOrg LegalSolutionInc

# configtxgen -outputAnchorPeersUpdate ClientOrgAnchors.tx -profile LegalSolutionChannel -channelID legalsolutionChannel -asOrg ClientOrg


    